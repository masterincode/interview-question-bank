List of class
1. ParkingLot
2. ParkingFloor
3. Gate
4. EntrancePoint
5. ExitPoint
6. ParkingTicket
7. Payment
8. PaymentInfo
9. ParkingDisplayBoard
10. Admin
11. ParkingAttendant        

List of Entity
1. Address
2. ParkingSpace
3. Account
4. Vehicle

List of Enum
1. PaymentType
2. ParkingSpaceType
3. ParkingTicketStatus
4. PaymentStatus

class ParkingSpace {
    int spaceId;
    boolean isSpaceAllocated;
    double costPerHour;
    //ParkingSpace (HAS-A) Vehicle
    Vehicle vehicle; //Simple Class
    //ParkingSpace (HAS-A) ParkingSpaceType
    ParkingSpaceType parkingSpaceType; // Enum BIKE, CAR, TRUCK, TEMPO
}

class ParkingFloor {
    String floorLevel; // Under Ground Assign like B2,B2,G,1,2,3,4,5
    boolean isFloorLevelFull;
    //ParkingFloor (HAS-A) List<ParkingSpace>
    List<ParkingSpace> parkingSpaces;
    //ParkingFloor (HAS-A) ParkingDisplayBoard
    ParkingDisplayBoard parkingDisplayBoard;
}

class ParkingLot {
    //ParkingLot (HAS-A) List<ParkingFloor>
    List<ParkingFloor> parkingFloors;
    //ParkingLot (HAS-A) List<EntrancePoint>
    List<EntrancePoint> entrances;
    //ParkingLot (HAS-A) List<ExitPoint>
    List<ExitPoint> entrances;
    // Address address; // Need TO check
    // String parkingLotName;

    public boolean isParkingSpaceAvailableForVehicle(Vehicle vehicle);

    public boolean updateParkingAttendant(ParkingAttendant parkingAttendant, int gateId);
}

class Gate {
    int gateld;
    ParkingAttendant parkingAttendant;
}

class EntrancePoint extends Gate {
    public ParkingTicket getParkingTicket(Vehicle vehicle);
}

class ExitPoint extends Gate {
    public ParkingTicket payForParking(ParkingTicket parkingTicket, PaymentType paymentType);
}

class ParkingDisplayBoard {
    Map<ParkingSpaceType, Integer> freeSpotsAvailableMap;

    public void updateFreeSpotsAvailable(ParkingSpaceType parkingSpaceType, int spaces);
)

class Admin extends Account {
    public boolean addParkingFloor(ParkingLot parkingLot, ParkingFloor floor);

    public boolean addParkingSpace(ParkingFloor floor, ParkingSpace parkingSpace);

    public boolean addParkingDisplayBoard(ParkingFloor floor, ParkingDisplayBoard parkingDi

}

class ParkingAttendant extends Account {
    Payment paymentService;

    public boolean processVehicleEntry(Vehicle vehicle);

    public PaymentInfo processPayment(ParkingTicket parkingTicket, PaymentType paymentType)
}

class ParkingTicket {
    int ticketId;
    int floorLevel;
    int spaceId;
    LocalDateTime vehicleEntryDateTime = LocalDateTime.now();

    ParkingSpaceType parkingSpaceType;
    double totalCost;
    ParkingTicketStatus parkingTicketStatus;

    public void updateTotalCost();

    LocalDateTime vehicleExitDateTime;

    public void setVehicleExitDateTime(LocalDateTime vehicleExitDateTime) {
        this.vehicleExitDateTime = vehicleExitDateTime;
    }
}
}

class Payment {
    public PaymentInfo makePayment(ParkingTicket parkingTicket, PaymentType paymentType);
}

public class PaymentInfo {
    double amount;
    Date paymentDate;
    int transactionId;
    ParkingTicket parkingTicket;
    PaymentStatus paymentStatus;
}

============Entity=================

class Address {
    String country;
    String state;
    String city;
    String street;
    String pinCode; //ZipCode

}

class Account {
    String name;
    String email;
    String password;
    String empId;
    Address address;
}

class Vehicle {
    String licenseNumber;
    VehicleType vehicleType;
    ParkingTicket parkingTicket;
    PaymentInfo paymentInfo;
}

==========ENUM==================

public enum PaymentType {
    CASH, CEDIT_CARD, DEBIT_CARD, UPI;
}

public enum ParkingSpaceType {
    BIKE_PARKING, CAR_PARKING, TRUCK_PARKING
}

public enum VehicleType {
    BIKE, CAR, TRUCK;
}

public enum ParkingTicketStatus {
    PAID, ACTIVE;
}

public enum PaymentStatus {
    UNPAID, PENDING, COMPLETED, DECLINED, CANCELLED, REFUNDED;
}















