JavaScript:

Closure: https://www.w3schools.com/js/js_function_closures.asp
var sum = function(a,b,c) {
    return {
        getSumTwo: function() { return a+b; },
        getSumThree: function() { return a+b+c; }
    }
}
var store = sum(3,4,5);
console.log(store.getSumTwo());
console.log(store.getSumThree());



Hoisting: https://scotch.io/tutorials/understanding-hoisting-in-javascript

Strict mode: https://javascript.info/strict-mode

Object properties & descriptors: https://javascript.info/property-descriptors

"this" keyword: https://www.geeksforgeeks.org/this-in-javascript/

Indexed & Keyed collections: http://webmobtuts.com/javascript/javascript-keyed-and-indexed-collections-array-map-and-set/

Map, reduce & filter: https://danmartensen.svbtle.com/javascripts-map-reduce-and-filter

Memory management: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Memory_Management

Error handling: https://www.tutorialspoint.com/javascript/javascript_error_handling.htm

Debugging: https://developers.google.com/web/tools/chrome-devtools/javascript/



Object Oriented JavaScript:



Basics: https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS

Abstraction, Encapsulation, Inheritance & Polymorphism: https://javascriptissexy.com/oop-in-javascript-what-you-need-to-know/

Inheritance & prototype  chain: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain

SOLID Principals: https://medium.com/@cramirez92/s-o-l-i-d-the-first-5-priciples-of-object-oriented-design-with-javascript-790f6ac9b9fa

JavaScript Principals Design Patterns: https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know



==============



TypeScript:



Abstraction, Encapsulation, Inheritance & Polymorphism: https://rachelappel.com/2015/01/02/write-object-oriented-javascript-with-typescript/

REST, default & optional params: https://howtodoinjava.com/typescript/functions-rest-optional-default-params/

Overloading: https://www.bennadel.com/blog/3339-using-method-and-function-overloading-in-typescript.htm

Iterators & decorators: https://www.typescriptlang.org/docs/handbook/iterators-and-generators.html

Intersections & union types: https://www.typescriptlang.org/docs/handbook/advanced-types.html

Decorators: https://www.typescriptlang.org/docs/handbook/decorators.html



===============



Angular:



Bootstrapping: https://angular.io/guide/bootstrapping

Lifecycle hooks: https://www.cuelogic.com/blog/angular-lifecycle

Routing: https://blog.angularindepth.com/the-three-pillars-of-angular-routing-angular-router-series-introduction-fb34e4e8758e

Router gaurd: https://codecraft.tv/courses/angular/routing/router-guards/

Dependency injection (injectors, providers): https://angular.io/guide/dependency-injection

Forms: https://angular-templates.io/tutorials/about/angular-forms-and-validations

Directives: https://www.sitepoint.com/practical-guide-angular-directives/

HostListener & HostBinding: https://codecraft.tv/courses/angular/custom-directives/hostlistener-and-hostbinding/

Pipe: https://scotch.io/tutorials/create-a-globally-available-custom-pipe-in-angular-2

Component communications: https://medium.com/@mirokoczka/3-ways-to-communicate-between-angular-components-a1e3f3304ecb

ViewChildren, ViewChild, ContentChildren & ContentChild: https://medium.com/@tkssharma/understanding-viewchildren-viewchild-contentchildren-and-contentchild-b16c9e0358e

Services: https://angular.io/tutorial/toh-pt4

HTTP Client: https://www.techiediaries.com/angular-http-client/

Web workers: https://medium.com/@damoresac/using-web-workers-on-angular-6-6fd0490d07b5

Base project structure & Webpack config: https://jasonwatmore.com/post/2019/04/24/angular-7-tutorial-part-2-create-base-project-structure-webpack-config

AOT: https://angular.io/guide/aot-compiler

Unit Tesitng: https://medium.com/@selvarajchinnasamyks/angular-7-unit-testing-97dccfdca900

Mock backend: https://jasonwatmore.com/post/2019/05/02/angular-7-mock-backend-example-for-backendless-development


Debugging: https://developers.google.com/web/tools/chrome-devtools/javascript/



Object Oriented JavaScript:


Basics: https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_JS

Abstraction, Encapsulation, Inheritance & Polymorphism: https://javascriptissexy.com/oop-in-javascript-what-you-need-to-know/

Inheritance & prototype  chain: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain

SOLID Principals: https://medium.com/@cramirez92/s-o-l-i-d-the-first-5-priciples-of-object-oriented-design-with-javascript-790f6ac9b9fa

JavaScript Principals Design Patterns: https://scotch.io/bar-talk/4-javascript-design-patterns-you-should-know



==============



TypeScript:



Abstraction, Enc…
[9:17 am, 10/03/2023] Tamim Wissen: Hi Mukund
[9:17 am, 10/03/2023] Mukund: Hi Tamim
[9:17 am, 10/03/2023] Tamim Wissen: Pls prepare these for angular round
[9:17 am, 10/03/2023] Mukund: when will be the interview ?
[9:17 am, 10/03/2023] Tamim Wissen: Will share more if I got any material
[9:17 am, 10/03/2023] Tamim Wissen: I will update you by today eod
[9:26 am, 10/03/2023] Mukund: ok
[10:16 am, 10/03/2023] Tamim Wissen:


@Input('input') inputValue: string; How to use the above input variable in the parent template?

How do you add external stylesheets to the angular app?

How do we add aunthentication token to each http request?

How do you send and receive the query param and path param in the routes?

What is the difference between ngOnInit() and constructor() of a component?

[10:18 am, 10/03/2023] Tamim Wissen:

What are the parameters to an observer?

How to inject dynamic script in angular?

Difference between Arrow functions and Native JS functions

Difference between call, apply and bind

What is the CSS Box model and what are its elements?
[10:18 am, 10/03/2023] Tamim Wissen: Mukund these are another set of questions
[10:18 am, 10/03/2023] Mukund: ok, thanks
[10:36 am, 10/03/2023] Tamim Wissen:
 You can give this snippet and ask the output (function immediateA(a) { return (function immediateB(b) { console.log(a); })(1); })(0); What is logged on console?



You can give this snippet and ask the output.
for (var i = 0; i < 3; i++)
{ setTimeout(function log() { console.log(i); // What is logged? }, 1000); }
What is logged on the console ?

const object = { who: 'World', greet() { return `Hello, ${this.who}!`; }, farewell: () => { return `Goodbye, ${this.who}!`; } }; console.log(object.greet()); // What is logged? console.log(object.farewell()); // What is logged? Answer: Hello World, Goodbye undefined.



Ask the candidate to create and save on jsfiddle.net or codesandbox.io. Make sure this jsfiddle/codesandbox link is included in the feedback commen…
[10:36 am, 10/03/2023] Tamim Wissen: you can prepare this also more focus
[10:37 am, 10/03/2023] Mukund: Ok