1. Concat
select first_name || ' ' || LAST_NAME as "Full Name" from EMPLOYEES;

2. Arithmetic Operation
select employee_id , salary as "Monthly Salary", salary + (salary * .10) as "After 10% Hike Monthly Salary" from employees;

3. Diff between SYSDATE and CURRENT_DATE
SYSDATE: OS where Oracle DB is insatlled
CURRENT_DATE: Place where the user session logged in.

4. Number format
SELECT salary,to_char(salary,'L99,999.99') as "Salary with $" FROM employees;

5. NVL(Exp1, Exp2) 
If(Exp1 === Null) ? Exp2 : Exp1

6. NVL2(Exp1, Exp2, Exp3)
If(Exp1 === Null) ? Exp3 : Exp2

7. NULLIF(Exp1, Exp2)
If(Exp1 === Exp2) ? NULL : Exp1

8. CASE ... WHEN
SELECT first_name, last_name, job_id, salary,
(CASE
	WHEN job_id = 'IT_PROG' THEN SALARY * 1.2
	WHEN (10000 > SALARY) THEN SALARY * 2
	ELSE 0
END AS "UPDATED SALARY")
FROM EMPLOYEES ;

9. LISTAGG FUNCTION
SELECT LISTAGG(city,'| ')within GROUP (ORDER BY city) AS cities FROM locations WHERE country_id = 'US';


Points to remember:
1. Index of string start from “1” not zero
2. NVL(Exp1, Exp2), Exp1 and Exp2 both has to be same data type.
3. NVL2(Exp1, Exp2, Exp3), Exp2 and Exp3 both has to be same data type.  
4. Where Clause filter ROWS and HAVING clause filters GROUPS data.
5. where can be used once but AND can be used multiple times
6. Inline Views

--1. Find manager name who is also an employee                       LEFT JOIN to also get those employees who MANAGER_ID is null
SELECT E.FIRST_NAME, D.FIRST_NAME AS MANAGER_NAME FROM HR.EMPLOYEE E LEFT JOIN HR.EMPLOYEE D on E.MANAGER_ID = D.EMPLOYEE_ID order by e.EMPLOYEE_ID;

--2. Find the premium subscription customer who has done txn of more than 1 Lacs in last 30 days.
SELECT CUSTOMER_ID FROM ORDERS WHERE DATE BETWEEN (DATE-30) AND SYSDATE GROUP BY CUSTOMERID HAVING SUM(AMOUNT) > 100000;

--3. Get the nth highest salary
SELECT MIN(SALARY) FROM (SELECT DISTINCT SALARY FROM HR.EMPLOYEES ORDER BY SALARY DESC) WHERE ROWNUM<=4;

SELECT salary FROM (SELECT salary, RANK() OVER (ORDER BY salary DESC) AS salary_rank FROM employees) WHERE salary_rank = 4

Q. Order of sql query execution
Ans: FWG - HSO
FROM and JOIN clauses: The first  step is to retrieve the data from the tables specified in the FROM and JOIN clauses of the query.
WHERE clause:          The second step is to filter   the data based on the conditions specified in the WHERE clause.
GROUP BY clause:       The third  step is to group    the filtered data based on the criteria specified in the GROUP BY clause.
HAVING clause:         The fourth step is to filter   the groups based on the conditions specified in the HAVING clause.
SELECT clause:         The fifth  step is to select   the columns or expressions specified in the SELECT clause of the query.
ORDER BY clause:       The final  step is to sort     the data based on the criteria specified in the ORDER BY clause.

Q. Difference between WHERE and HAVING
Ans:
1. The WHERE clause filters individual rows based on a condition, while the HAVING clause filters groups of rows based on a condition.
2. The WHERE clause is typically used with SELECT, UPDATE, and DELETE statements, while
   the HAVING clause is typically used with GROUP BY clauses in SELECT statements.

Q. Difference between function and procedure
Ans:

Q. What is the difference between a Primary Key & a Unique Key?
Ans: Primary key is used to identify each table row uniquely, while a Unique Key prevents duplicate values in a table column.
Given below are few differences:
  • The primary key cannot hold null value at all while Unique key allows multiple null values.
  • The primary key is a clustered index while a unique key is a non-clustered index.

Q. What is meant by an index?
Ans: An index is a schema object, which is created to search the data efficiently within the table.
     Indexes are usually created on certain columns of the table, which are accessed the most.
     Indexes can be clustered or non-clustered.

Q. What are the set operators UNION, UNION ALL, MINUS & INTERSECT meant to do?
Ans: Set operator facilitates the user to fetch the data from two or more than two tables at once if the columns and relative data types are same in the source tables.
    • UNION operator returns all the rows from both the tables except the duplicate rows.
    • UNION ALL returns all the rows from both the tables along with the duplicate rows.
    • MINUS returns rows from the first table, which does not exist in the second table.
    • INTERSECT returns only the common rows in both the tables.

Q. Type of JOIN in SQL
Ans:
1. Inner join: Returns only the matching records from both tables based on the join condition.
2. Outer join: Returns all records from both tables, including the unmatched records.

   There are three types of outer join: left outer join, right outer join, and full outer join.
    a. Left outer join: Returns all records from the left table and the matching records from the right table. If there are no matches, the result contains NULL values for the right table.
    b. Right outer join: Returns all records from the right table and the matching records from the left table. If there are no matches, the result contains NULL values for the left table.
    c. Full outer join: Returns all records from both tables. If there are no matches, the result contains NULL values for the missing table.




-- Q-1 Show Me The List Of Maximum Salary Paid By Each Department, Count Of Employees From Each Department And Department Name
-- Q-2 Show Me The List Of Avg Salary Paid By Each Department And Their Department Name.
-- Q-3 Show Me The List of employees name as of hire date is 17 Sep 2003
-- Q-4 Total Salary Distributed By Comapany For Each Department Wise Report :
-- Q-5 Show Data Of Department Whose Having Employess More Than 10;
-- Q-6 List The Number Of EMPLOYEES In Each DEPARTMENT, Except The SHIPPING, Sorted High To Low. Only Include DEPARTMENT With 5 Or More EMPLOYEES


SELECT D.DEPARTMENT_NAME,  MAX(E.SALARY) FROM EMPLOYEE E
JOIN DEPARTMENT D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID
GROUP BY D.DEPARTMENT_NAME;

SELECT D.DEPARTMENT_NAME, AVG(E.SALARY)  FROM EMPLOYEE E 
JOIN DEPARTMENT D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID
GROUP BY D.DEPARTMENT_NAME;

SELECT D.DEPARTMENT_NAME, SUM(E.SALARY) FROM EMPLOYEE E 
JOIN DEPARTMENT D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID
GROUP BY D.DEPARTMENT_NAME;

SELECT D.DEPARTMENT_NAME, COUNT(D.DEPARTMENT_NAME) FROM EMPLOYEE E 
JOIN DEPARTMENT D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID
GROUP BY D.DEPARTMENT_NAME HAVING COUNT(D.DEPARTMENT_NAME) > 10;

SELECT D.DEPARTMENT_NAME, COUNT(D.DEPARTMENT_NAME) AS 'Number Of EMPLOYEES' FROM EMPLOYEE E JOIN DEPARTMENT D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID
WHERE D.DEPARTMENT_NAME <> 'Shipping'
GROUP BY D.DEPARTMENT_NAME HAVING COUNT(D.DEPARTMENT_NAME) >= 5
order by COUNT(D.DEPARTMENT_NAME) desc;


-- Q. Show me the Name, Dept Name , salary of employee who salary has greater than avg of its dept
-- APPROACH 1
SELECT E.FIRST_NAME, D.DEPARTMENT_NAME,E.SALARY FROM EMPLOYEES E
JOIN DEPARTMENTS D ON E.DEPARTMENT_ID = D.DEPARTMENT_ID
JOIN
( SELECT D.DEPARTMENT_ID, AVG(E.SALARY) AS AVG_SALARY  FROM EMPLOYEES E JOIN DEPARTMENTS D
ON E.DEPARTMENT_ID = D.DEPARTMENT_ID GROUP BY D.DEPARTMENT_ID) TB
ON D.DEPARTMENT_ID = TB.DEPARTMENT_ID
WHERE E.SALARY > TB.AVG_SALARY;










